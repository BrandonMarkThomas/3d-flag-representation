jQuery(document).ready(function ($) {
	
	var initialValue = 100;
	var tooltip = '<span class="tt"><span class="text">'+getDescript(initialValue)+'</span></span>';
	var target;
	
	$("#speed").slider({
		min: 25,
		max: 300,
		value: initialValue,
		animate: true,
		range: 'min',
		create: function(event, ui) {
			initTooltip(ui);
		},
		slide: function(event, ui) {
			setSpeed(ui);
		}
	});	
	
	function initTooltip(ui){
		target = ui.handle || $('.ui-slider-handle');  
	}
	
	function setSpeed(ui){
		windStrength = ui.value;
		tooltip = '<span class="tt"><span class="text">'+getDescript(ui.value)+'</span></span>';
		target = ui.handle || $('.ui-slider-handle');
		$(target).html(tooltip); 		
	}
	
	function getDescript(speed){
		if(speed < 50) return 'calm';
		if(speed < 100) return 'light';
		if(speed < 150) return 'fair';
		if(speed < 200) return 'heavy';
		return 'gusty';
	}
	
	$(".ui-slider").mouseenter(function(){
		controls.enabled = false;
		$(target).html(tooltip);
	});
	
	$(".ui-slider").mouseleave(function(){
		controls.enabled = true;
		$(".tt").remove();
	});

});