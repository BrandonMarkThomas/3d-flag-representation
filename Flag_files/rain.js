var particleCount;
var particleSystem;

function rain(){	
	// create the particle variables
		particleCount = 3600,
	    particles = new THREE.Geometry(),
		pMaterial = new THREE.ParticleBasicMaterial({
			color: 0xFFFFFF,
			size: 10,
			map: THREE.ImageUtils.loadTexture(
				"./images/raindrop.png"
			),
			blending: THREE.AdditiveBlending,
			transparent: true,
			opacity: 0.5
		});
	
	// now create the individual particles
	for(var p = 0; p < particleCount; p++) {
	
		// create a particle with random
		// position values, -250 -> 250
		var pX = Math.random() * window.innerWidth*2 - window.innerWidth,
			pY = Math.random() * window.innerHeight*3 - window.innerHeight,
			pZ = Math.random() * window.innerWidth*2 - window.innerWidth,
		    particle = new THREE.Vector3(pX, pY, pZ)
				
		// create a velocity vector
		particle.velocity = new THREE.Vector3(
			0,				// x
			-Math.random(),	// y
			0);				// z

		// add it to the geometry
		particles.vertices.push(particle);
	}
	
	// create the particle system
		particleSystem = new THREE.ParticleSystem(
		particles,
		pMaterial);
	
	particleSystem.sortParticles = true;
	
	return particleSystem;
}
	// animation loop
	function updateRain() {
		
		// add some rotation to the system
		particleSystem.rotation.y += 0.001;
		
		var pCount = particleCount;
		while(pCount--) {
			// get the particle
			var particle = particles.vertices[pCount];
			
			// check if we need to reset
			if(particle.y < -200) {
				particle.y = 200;
				particle.velocity.y = 0;
			}
			
			// update the velocity
			particle.velocity.y -= Math.random() * .1;
			
			// and the position
			particle.addVectors( 
				particle,
				particle.velocity);
		}
		
		// flag to the particle system that we've
		// changed its vertices. This is the
		// dirty little secret.
		particleSystem.geometry.__dirtyVertices = true;
		
	}