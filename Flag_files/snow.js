
			var particle;
			var particleImage = new Image();

			var particles = new THREE.Geometry(),
				pMaterial = new THREE.ParticleBasicMaterial({
				color: 0xFFFFFF,
				size: 15,
				map: THREE.ImageUtils.loadTexture(
					"./images/snow.jpg"
				),
				blending: THREE.AdditiveBlending,
				transparent: true
			});
	
		
		
			function snow() {
					
				//var material = new THREE.ParticleBasicMaterial( { map: new THREE.Texture(particleImage) } );
					
				for (var i = 0; i < 500; i++) {

					particle = new THREE.Vector3();
					particle.x = Math.random() * 2000 - 1000;
					particle.y = Math.random() * 2000 - 1000;
					particle.z = Math.random() * 2000 - 1000;
					//particle.scale.x = particle.scale.y =  1;
					
					particles.vertices.push(particle); 
				}
				particleSystem = new THREE.ParticleSystem(
				particles,
				pMaterial);
				
				particleSystem.sortParticles = true;
	
				return particleSystem;
			}

			//

			function updateSnow() {

			for(var i = 0; i < particles.length; i++)
				{

					var particle = particles[i]; 
					particle.updatePhysics(); 
	
					with(particle.position)
					{
						if(y<-1000) y+=2000; 
						if(x>1000) x-=2000; 
						else if(x<-1000) x+=2000; 
						if(z>1000) z-=2000; 
						else if(z<-1000) z+=2000; 
					}				
				}
				
			}

		