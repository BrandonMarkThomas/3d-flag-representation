
	function snow() {
	
		this.isInit = false;
		this.MASS = 0.005;
		
		this.init = function() {
			
			// modify lighting
			this.HSL = light.color.getHSL();
			light.color.setHSL(this.HSL.h, this.HSL.s, 0.6);
		
			if(this.isInit === true) return;
		
			// create the particle variables
			this.particleCount = 6000,
			this.particles = new THREE.Geometry(),
			
			this.pMaterial = new THREE.ParticleBasicMaterial({
				color: 0xFFFFFF,
				size: 25,
				map: THREE.ImageUtils.loadTexture(
					"./images/snow.png"
				),
				blending: THREE.AdditiveBlending,
				transparent: true,
				opacity: 0.9
			});

			// now create the individual particles
			for (var p = 0; p < this.particleCount; p++) {

				// create a particle with random
				// position values
				var pX = (Math.random() * 10000) - 5000,
					pY = Math.random() * window.innerHeight * 6 - window.innerHeight,
					pZ = (Math.random() * 10000) - 5000;
					this.particle = new THREE.Vector3(pX, pY, pZ);	
				
				// create a velocity vector
				this.particle.velocity = new THREE.Vector3(
					(windForce.x * this.MASS), // x
					-Math.random() * 0.01, // y
					(windForce.z * this.MASS)); // z
				
				//this.particle.velocity.z = arrow.direction.z * this.MASS;
				// add it to the geometry
				this.particles.vertices.push(this.particle);
			
			}
			
			// create the particle system
			this.particleSystem = new THREE.ParticleSystem(
				this.particles,
				this.pMaterial);

			this.particleSystem.sortParticles = true;
			
			this.isInit = true;
		}
	
		this.system = function(){
			return this.particleSystem;
		}
	
	    this.update = function() {
			// add some rotation to the system
			//particleSystem.rotation.y += 0.01;
			
			var pCount = this.particleCount;
			while(pCount--) {
				// get the particle
				this.particle = this.particles.vertices[pCount];
				
				// check if we need to reset
				if(this.particle.y < -10) {
					this.particle.y = (window.innerHeight*6)+(Math.random() * 100);
					this.particle.velocity.y = 0;
					this.particle.x = (Math.random() * 10000) - 5000;
					this.particle.z = (Math.random() * 10000) - 5000;

				}
				
				// update the velocity
				this.particle.velocity.y -= Math.random() * 0.001;
				//this.particle.velocity.x = (windForce.x * this.MASS);
				//this.particle.velocity.z = (windForce.z * this.MASS);

				// and the position
				this.particle.addVectors( 
					this.particle,
					this.particle.velocity);
			}
		
			// flag to the particle system that we've
			// changed its vertices.
			this.particleSystem.geometry.__dirtyVertices = true;
		}
}