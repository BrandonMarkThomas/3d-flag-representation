
	function weatherSystem(scene){
		
		this.scene = scene;
		this.s = false;
		this.type = null;
		
		this.set = function(weather){
			this.s = true;
			this.type = weather;
			weather.init();
			this.scene.add(this.getWeather());
		}
		
		this.getWeather = function(){
			return this.type.system();
		}

		this.update = function(){
			this.type.update();
		}
		
		this.swap = function(w){
			if(this.type != w){
				if(this.type !== null){
					this.scene.remove(this.getWeather());
				}
				this.set(w);
			}
		}
		
		this.isSet = function(){
			return this.s;
		}
	}
	
	
	
	
	
	
	